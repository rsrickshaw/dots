" https://rsrickshaw.gitlab.io/vimrc

" Plugins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
  Plug 'whatyouhide/vim-gotham'        " Color scheme
  Plug 'pangloss/vim-javascript'       " JavaScript
  Plug 'airblade/vim-gitgutter'        " Git markers
  Plug 'machakann/vim-highlightedyank' " Highlight copied lines
call plug#end()

" General
set termguicolors  " Use terminal GUI colors.
colorscheme gotham " Color scheme.
set number         " Show line numbers.
set cursorline     " Highlight current line.
set title          " Show filename in title bar.
set mouse=a        " Enable mouse.

" Searches
set incsearch  " Highlight searches while typing.
set ignorecase " Ignore search case...
set smartcase  " ...unless uppercase.
set gdefault   " Replace all matches by default when performing find-and-replace.

" Invisibles
set list                                                  " Show invisibles.
set listchars=tab:→\ ,nbsp:␣,trail:•,precedes:«,extends:» " Use these characters for invisibles.

" Indentation
set expandtab    " Insert spaces when pressing tab.
set tabstop=2    " Use two spaces for expandtab.
set shiftwidth=2 " Shift two spaces at a time when indenting.

" GitGutter
let g:gitgutter_enabled=1 " Enable GitGutter

" Dynamic Cursor
let &t_SI="\<Esc>[5 q" " Line cursor for INSERT mode
let &t_SR="\<Esc>[4 q" " Underline cursor for REPLACE mode
let &t_EI="\<Esc>[1 q" " Block cursor for other modes

" Spelling
" ctrl+l autocorrects last spelling error.
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u